import api
import permissions
import util

# pip install requests
# requires python-docx and reportlab

# service = sys.argv[1]
# username = sys.argv[2]
# password = sys.argv[3]
# print 'Service: ' + service
# print 'Username: ' + username
# print 'Password: ' + password

service = 'http://qa.sec.covata.com'

admin_email = 'user1@test.covata.com'
admin_pw = 'password1'

originator_email = 'natalie.nb@covata.com'
originator_pw = 'Password1'

collaborator_email = 'natalie.nb+collaborator@covata.com'
collaborator_pw = 'Password1'

other_email1 = 'natalie.nb+other1@covata.com'
other_email2 = 'natalie.nb+other2@covata.com'

# ---------------------- ADMIN -------------------------

# values = {
#     'grant_type': 'password',
#     'username': admin_email,
#     'password': admin_pw
# }
# api.get_oauth_token(service, values)

# api.create_admin_user('natalie.nb@covata.com')
# api.create_organisation('Android Admin Department', 'natalie.nb@covata.com', 'natalie.nb@covata.com', 1000)
# api.create_organisation('Android DEV Department', 'natalie.nb@covata.com', 'natalie.nb@covata.com', 1000)
# api.create_organisation('Android Finance Department', 'natalie.nb@covata.com', 'natalie.nb@covata.com', 1000)
# api.create_organisation('Android Manage Department', 'natalie.nb@covata.com', 'natalie.nb@covata.com', 1000)
# api.create_organisation('Android QA Department', 'natalie.nb@covata.com', 'natalie.nb@covata.com', 1000)
# api.create_organisation('Android Service Department', 'natalie.nb@covata.com', 'natalie.nb@covata.com', 1000)

# ---------------------- ORIGINATOR -------------------------

values = {
    'grant_type': 'password',
    'username': originator_email,
    'password': originator_pw
}
api.get_oauth_token(service, values)
api.get_discovery()
my_id = api.get_me()

root_collection_id = '0'
organisations = api.get_organisations()
for x in range(0, len(organisations)):
    org = organisations[x]
    org_id = org['id']

    if (org['name'] == 'Cisco') or (org['name'] == 'Test Blitz'):
        continue
    else:
        print '======' + org['name'] + ': ' + org_id + '======'

    if org['name'] == 'Android Admin Department':
        plan_id = api.create_plan(org_id, 'Covata Basic Plan', 'This is Covata Basic plan', 500)
        if plan_id is not None:
            api.update_admin_org_user(org_id, my_id, plan_id, False)
            api.add_org_user(org_id, collaborator_email, plan_id, True, True)

        api.set_home_organisation(org_id)
        folder_id = api.create_folder(org_id, 'complexFolder', root_collection_id)
        folder1_id = api.create_folder(org_id, 'folder1', folder_id)
        folder2_id = api.create_folder(org_id, 'folder2', folder1_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.VIEW)

        folder_id = api.create_folder(org_id, 'displayFolderContent', root_collection_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.VIEW)

        api.create_folder(org_id, 'folder01_AM', root_collection_id)
        api.create_folder(org_id, 'folder02_AM', root_collection_id)
        folder_id = api.create_folder(org_id, 'folderForSharing', root_collection_id)

        api.create_folder(org_id, 'folderForView', root_collection_id)
        api.create_folder(org_id, 'folderNotShared', root_collection_id)

        folder_id = api.create_folder(org_id, 'folderSharedWithManyPeople', root_collection_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.VIEW)
        api.share_item(folder_id, other_email1, True, permissions.Permissions.VIEW)
        api.share_item(folder_id, other_email2, True, permissions.Permissions.VIEW)

        folder_id = api.create_folder(org_id, 'folderWithDownloadPermission', root_collection_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.DOWNLOAD)

        folder_id = api.create_folder(org_id, 'folderWithManagePermission', root_collection_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.MANAGE)

        folder_id = api.create_folder(org_id, 'folderWithModifyPermission', root_collection_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.MODIFY)

        api.create_folder(org_id, 'folderWithModifyToRemove', root_collection_id)

        object_id = api.create_folder(org_id, 'folderWithUploadPermission', root_collection_id)
        api.share_item(object_id, collaborator_email, True, permissions.Permissions.UPLOAD)

        folder_id = api.create_folder(org_id, 'folderWithViewPermission', root_collection_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.VIEW)

        api.create_folder(org_id, 'scrollFolder', root_collection_id)

        api.upload_text_file(org_id, 'aFile.txt', root_collection_id)

        # probably have to have a big pdf file in this directory to upload, instead of creating one
        api.upload_pdf_file(org_id, 'bigFileToCancelView.pdf', root_collection_id)

        api.upload_doc_file(org_id, 'doNotTouchTestHistoryFile.doc', root_collection_id)

        object_id = api.upload_text_file(org_id, 'fileSharedWithinDate.txt', root_collection_id)
        api.set_share_period(object_id, util.get_datetime_now(), util.get_datetime_later())

        label_id = api.create_label(org_id, 'android4_HR04_label')
        label_id = api.create_label(org_id, 'android4_HR01_label')
        group_id = api.create_group(org_id, 'android4_HR01_group')
        api.update_group(org_id, group_id, collaborator_email, label_id)

        object_id = api.upload_text_file(org_id, 'fileToLabel.txt', root_collection_id)
        api.share_item(object_id, collaborator_email, False, permissions.Permissions.VIEW)

        api.upload_text_file(org_id, 'fileToRename.txt', root_collection_id)

        object_id = api.upload_pdf_file(org_id, 'fileToSharedWithTime.pdf', root_collection_id)
        api.share_item(object_id, collaborator_email, False, permissions.Permissions.VIEW)

        api.upload_text_file(org_id, 'fileToTest_AM.txt', root_collection_id)

        object_id = api.upload_text_file(org_id, 'fileWithDownloadPermission.txt', root_collection_id)
        api.share_item(object_id, collaborator_email, False, permissions.Permissions.DOWNLOAD)

        object_id = api.upload_doc_file(org_id, 'fileWithModifyPermission.docx', root_collection_id)
        api.share_item(object_id, collaborator_email, False, permissions.Permissions.MODIFY)

        api.upload_doc_file(org_id, 'fileWithSharingTimeToRemove.doc', root_collection_id)

        object_id = api.upload_text_file(org_id, 'fileWithViewPermission.txt', root_collection_id)
        api.share_item(object_id, collaborator_email, False, permissions.Permissions.VIEW)

        api.upload_text_file(org_id, 'testEndOfPage.rtf', root_collection_id)

        # Files and folders to remove
        api.create_folder(org_id, 'folderToRemoveInLandingPage', root_collection_id)
        folder_id = api.create_folder(org_id, 'folderForOwnerToRemove', root_collection_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.VIEW)
        folder_id = api.create_folder(org_id, 'folderWithViewToRemove', root_collection_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.VIEW)
        folder_id = api.create_folder(org_id, 'folderWithDownloadToRemove', root_collection_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.DOWNLOAD)
        folder_id = api.create_folder(org_id, 'folderWithUploadToRemove', root_collection_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.UPLOAD)
        folder_id = api.create_folder(org_id, 'folderWithManageToRemove', root_collection_id)
        api.share_item(folder_id, collaborator_email, True, permissions.Permissions.MANAGE)

        api.upload_text_file(org_id, 'fileToRemoveInItemDetailPage.txt', root_collection_id)
        object_id = api.upload_pdf_file(org_id, 'fileForOwnerToRemove.pdf', root_collection_id)
        api.share_item(object_id, collaborator_email, False, permissions.Permissions.VIEW)
        object_id = api.upload_text_file(org_id, 'fileWithManageToRemove.txt', root_collection_id)
        api.share_item(object_id, collaborator_email, False, permissions.Permissions.MANAGE)
        object_id = api.upload_text_file(org_id, 'fileWithViewToRemove.txt', root_collection_id)
        api.share_item(object_id, collaborator_email, False, permissions.Permissions.VIEW)
        object_id = api.upload_text_file(org_id, 'fileWithDownloadToRemove.txt', root_collection_id)
        api.share_item(object_id, collaborator_email, False, permissions.Permissions.DOWNLOAD)

    if org['name'] == 'Android DEV Department':
        plan_id = api.create_plan(org_id, 'Covata Basic Plan', 'This is Covata Basic plan', 500)
        if plan_id is not None:
            api.update_admin_org_user(org_id, my_id, plan_id, False)
            api.add_org_user(org_id, collaborator_email, plan_id, True, True)
        api.upload_pdf_file(org_id, '2.pdf', root_collection_id)
        api.upload_doc_file(org_id, 'a big word file 02.doc', root_collection_id)
        util.get_image('Covata.png', 'https://www.covata.com/wp-content/uploads/2015/06/Covata.png')
        api.upload_file(org_id, 'Covata.png', root_collection_id)

    if org['name'] == 'Android Manage Department':
        plan_id = api.create_plan(org_id, 'Covata Basic Plan', 'This is Covata Basic plan', 500)
        if plan_id is not None:
            api.update_admin_org_user(org_id, my_id, plan_id, False)
            api.add_org_user(org_id, collaborator_email, plan_id, True, True)
        object_id = api.create_folder(org_id, 'folder01_AM', root_collection_id)
        api.share_item(object_id, collaborator_email, True, permissions.Permissions.VIEW)
        object_id = api.create_folder(org_id, 'folder02_AM', root_collection_id)
        api.share_item(object_id, collaborator_email, True, permissions.Permissions.VIEW)
        object_id = api.upload_text_file(org_id, 'fileToTest_AM.txt', root_collection_id)
        api.share_item(object_id, collaborator_email, False, permissions.Permissions.VIEW)

    if org['name'] == 'Android Finance Department':
        plan_id = api.create_plan(org_id, 'Covata Basic Plan', 'This is Covata Basic plan', 500)
        if plan_id is not None:
            api.update_admin_org_user(org_id, my_id, plan_id, False)
        api.add_org_user(org_id, collaborator_email, plan_id, True, False)

    if org['name'] == 'Android QA Department':
        plan_id = api.create_plan(org_id, 'Covata Basic Plan', 'This is Covata Basic plan', 500)
        if plan_id is not None:
            api.update_admin_org_user(org_id, my_id, plan_id, False)
        api.add_org_user(org_id, collaborator_email, plan_id, True, False)

    if org['name'] == 'Android Service Department':
        plan_id = api.create_plan(org_id, 'Covata Basic Plan', 'This is Covata Basic plan', 500)
        if plan_id is not None:
            api.update_admin_org_user(org_id, my_id, plan_id, False)
        api.add_org_user(org_id, collaborator_email, plan_id, True, False)

# ---------------------- COLLABORATOR -------------------------

values = {
    'grant_type': 'password',
    'username': collaborator_email,
    'password': collaborator_pw
}
api.get_oauth_token(service, values)
api.get_discovery()
my_id = api.get_me()

root_collection_id = '0'
organisations = api.get_organisations()
for x in range(0, len(organisations)):
    org = organisations[x]
    org_id = org['id']
    print '======' + org['name'] + ': ' + org_id + '======'

    if org['name'] == 'Android Admin Department':
        api.set_home_organisation(org_id)
        api.create_folder(org_id, "Folder01_Android5", root_collection_id)
        util.get_image("Covata.png", "https://www.covata.com/wp-content/uploads/2015/06/Covata.png")
        api.upload_file(org_id, "Covata.png", root_collection_id)
        api.create_folder(org_id, 'folderToRemoveInLandingPage02', root_collection_id)
        api.upload_text_file(org_id, 'fileToRemoveInItemDetailPage02.txt', root_collection_id)

   if org['name'] == 'Android Manage Department':
        folder_id = api.create_folder(org_id, 'complexFolder', root_collection_id)
        folder_id = api.create_folder(org_id, 'folder1', folder_id)
        folder_id = api.create_folder(org_id, 'folder2', folder_id)
        folder_id = api.create_folder(org_id, 'folder_SharedByAndroid5', root_collection_id)
        folder1_id = api.create_folder(org_id, 'folder01', folder_id)
        folder2_id = api.create_folder(org_id, 'folder02', folder1_id)
        api.upload_text_file(org_id, 'testFile02.txt', folder1_id)
        api.share_item(folder_id, originator_email, True, permissions.Permissions.VIEW)
        folder1_id = api.create_folder(org_id, 'folder1', folder_id)
        folder2_id = api.create_folder(org_id, 'folder2', folder_id)
        api.upload_text_file(org_id, 'testFile02.txt', folder_id)
        api.create_folder(org_id, 'folder2', folder1_id)
        api.upload_text_file(org_id, 'testFile02.txt', folder1_id)
        object_id = api.upload_doc_file(org_id, 'file_SharedByAndroid5.docx', root_collection_id)
        api.share_item(object_id, originator_email, False, permissions.Permissions.VIEW)
