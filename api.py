import auth
import requests
import os
import util

service = None
content_service = None


def get_oauth_token(s, values):
    global service
    service = s
    r = requests.post(service + '/api/oauth/token', data=values, headers=auth.create_oauth_header())
    auth.save_tokens(r.json())
    print 'Getting OAuth Token...' + str(r.status_code)


def get_discovery():
    r = requests.get(service + '/api/v1', headers=auth.create_auth_header())
    global content_service
    content_service = r.json()['contentServiceURL']
    print 'Getting discovery...' + str(r.status_code)
    # print 'Content Service: ' + content_service


def get_me():
    r = requests.get(service + '/api/v1/users/me', headers=auth.create_auth_header())
    return r.json()['id']


def create_folder(orgId, name, parentId):
    new_folder_request = {
        'name': name,
        'parentId': parentId
    }
    r = requests.post(
        service + '/api/v1/organisations/' + orgId + '/collections',
        json=new_folder_request, headers=auth.create_auth_header())

    # c = 'curl -X POST ' + service + '/api/v1/organisations/' + orgId + '/collections -H "' + auth.auth_header() + \
    #     '" -H "Content-Type: application/json" -d \'' + \
    #     '{"name":"' + name + '", "parentId":"' + parentId + '"}\''
    # print c
    # os.system(c)
    print 'Creating folder ' + name + '...' + str(r.status_code)
    if r.status_code == requests.codes.ok:
        return r.json()['id']
    else:
        print r


def upload_file(orgId, name, parentId):
    # print 'Creating file'
    size = os.stat(name).st_size
    print 'File size: ' + str(size)

    r = requests.post(service + '/api/v1/keys', json={}, headers=auth.create_auth_header())
    upload_key = r.json()['keys'][0]['id']
    print 'Upload key: ' + upload_key
    print 'Getting upload key...' + str(r.status_code)

    upload_init_request = {
        'contentSize': size,
        'keyId': upload_key,
        'name': name,
        'parentId': parentId
    }
    r = requests.post(service + '/api/v1/organisations/' + orgId + '/objects', json=upload_init_request,
                      headers=auth.create_auth_header())
    print 'Initialise upload...' + str(r.status_code)
    object_id = None
    if r.status_code == requests.codes.ok:
        object_id = r.json()['id']
        print 'Object id: ' + object_id
    else:
        print r

    if r.status_code == requests.codes.ok:
        print 'Uploading ' + name + '...'
        os.system('curl -X POST ' + content_service + '/api/v1/objects/' + object_id +
                  '/contents?format=plaintext -H "' + auth.auth_header() +
                  '" -H "Content-Type: multipart/form-data" -F "totalFileSizeBytes=' + str(size) +
                  '" -F "data=@' + name + '"')
    os.remove(name)
    return object_id


def upload_text_file(org_id, name, parent_id):
    util.create_text_file(name)
    object_id = upload_file(org_id, name, parent_id)
    return object_id


def upload_pdf_file(org_id, name, parent_id):
    util.create_pdf_file(name)
    object_id = upload_file(org_id, name, parent_id)
    return object_id


def upload_doc_file(org_id, name, parent_id):
    util.create_doc_file(name)
    object_id = upload_file(org_id, name, parent_id)
    return object_id


def create_organisation(name, contact_email, admin_email, quota):
    request = {
        'name': name,
        'contactEmail': contact_email,
        'adminEmail': admin_email,
        'quota': quota
    }
    r = requests.post(service + '/api/v1/organisations', json=request, headers=auth.create_auth_header())
    print 'Create organisation: ' + name + '...' + str(r.status_code)


def create_admin_user(email):
    request = {
        'email': email,
        'admin': True
    }
    r = requests.put(service + '/api/v1/users', json=request, headers=auth.create_auth_header())
    print 'Create admin user: ' + email + ' ' + str(r.status_code)


def get_organisations():
    r = requests.get(service + '/api/v1/users/me/organisations', headers=auth.create_auth_header())
    return r.json()['organisations']


def create_plan(org_id, name, desc, quota):
    request = {
        'name': name,
        'description': desc,
        'quota': quota
    }
    r = requests.post(service + '/api/v1/organisations/' + org_id + '/plans',
                      json=request, headers=auth.create_auth_header())
    if r.status_code == requests.codes.ok:
        print 'Create ' + name + ' plan for org: ' + str(org_id) + ', plan id: ' + r.json()['id'] + '...' + str(
            r.status_code)
        return r.json()['id']
    else:
        print 'Create ' + name + ' plan for org: ' + str(org_id) + '...' + str(r.status_code)
        return None


def update_admin_org_user(org_id, user_id, plan_id, is_collaborator):
    role = ['ROLE_ORIGINATOR', 'ROLE_ORGANISATION_ADMIN']
    if is_collaborator is True:
        # This is how it was set before, collaborator has originator role but without org admin role
        role = ['ROLE_ORIGINATOR']
    request = {
        'roles': role,
        'planId': plan_id
    }
    r = requests.put(service + '/api/v1/organisations/' + org_id + '/users/' + user_id,
                     json=request, headers=auth.create_auth_header())
    print 'Update my role to: ' + str(role) + '...' + str(r.status_code)


def set_home_organisation(org_id):
    request = {
        'homeOrganisationId': org_id
    }
    r = requests.put(service + '/api/v1/users/me', json=request, headers=auth.create_auth_header())
    print 'Set org: ' + str(org_id) + ' as home org...' + str(r.status_code)


def add_org_user(org_id, email, plan_id, is_collaborator, is_org_admin):
    role = ['ROLE_ORIGINATOR']
    if is_collaborator is True:
        role = ['ROLE_COLLABORATOR']
    if is_org_admin is True:
        role.append('ROLE_ORGANISATION_ADMIN')
    request = {
        'email': email,
        'planId': plan_id,
        'roles': role
    }
    if is_collaborator is True:
        request.pop('planId')
    r = requests.post(service + '/api/v1/organisations/' + org_id + '/users',
                      json=request, headers=auth.create_auth_header())
    print 'Add org user ' + email + ' with role: ' + str(role) + '...' + str(r.status_code)


def share_item(object_id, collaborator_email, is_folder, permission):
    collaborator = {
        'email': collaborator_email,
        'permissionSet': {'id': permission}
    }
    request = {
        'collaborators': {'list': [collaborator]}
    }
    if is_folder is True:
        r = requests.put(service + '/api/v1/collections/' + str(object_id), json=request,
                         headers=auth.create_auth_header())
        print 'Sharing folder with ' + collaborator_email + '...' + str(r.status_code)
    else:
        r = requests.put(service + '/api/v1/objects/' + str(object_id), json=request, headers=auth.create_auth_header())
        print 'Sharing file with ' + collaborator_email + '...' + str(r.status_code)


# yyyy-MM-ddTHH:mm:ss.sssZ
def set_share_period(object_id, share_start_time, share_end_time):
    request = {
        'shareStartTime': share_start_time,
        'shareEndTime': share_end_time
    }
    r = requests.put(service + '/api/v1/objects/' + str(object_id), json=request,
                     headers=auth.create_auth_header())
    print 'Set share period for ' + str(object_id) + '...' + str(r.status_code)


def create_label(org_id, name):
    request = {
        'name': name
    }
    r = requests.post(service + '/api/v1/organisations/' + str(org_id) + '/labels',
                      json=request, headers=auth.create_auth_header())
    print 'Create label: ' + name + '...' + str(r.status_code)
    if r.status_code == requests.codes.ok:
        return r.json()['id']
    else:
        print r


def apply_label(object_id, label_id):
    request = {
        'labelId': label_id
    }
    r = requests.put(service + '/api/v1/objects/' + str(object_id), json=request, headers=auth.create_auth_header())
    print 'Setting label...' + str(r.status_code)


def create_group(org_id, name):
    request = {
        'name': name
    }
    r = requests.post(service + '/api/v1/organisations/' + str(org_id) + '/groups',
                      json=request, headers=auth.create_auth_header())
    print 'Create group: ' + name + '...' + str(r.status_code)
    if r.status_code == requests.codes.ok:
        return r.json()['id']
    else:
        print r


def update_group(org_id, group_id, member, label_id):
    members = {
        'add': [{'email': member}]
    }
    labels = {
        'add': [{'id': label_id}]
    }
    request = {
        'members': members,
        'labels': labels
    }
    r = requests.put(service + '/api/v1/organisations/' + str(org_id) + '/groups/' + str(group_id),
                     json=request, headers=auth.create_auth_header())
    print 'Update group: add member ' + member + ', label ' + str(label_id) + '...' + str(r.status_code)
