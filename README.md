This is a python script for setting up data for Android automation tests

Libraries required:

* Requests: HTTP for Humans (http://docs.python-requests.org/)
* python-docx (https://github.com/python-openxml/python-docx)
* ReportLab (http://www.reportlab.com/)