import base64

access_token = None
token_type = None
refresh_token = None


def create_oauth_header():
    s = "androidClient:androidClientSecret"
    encoded = base64.b64encode(s)
    return {
        'Authorization': 'Basic ' + encoded
    }


def save_tokens(response_json):
    global access_token
    access_token = response_json['access_token']
    # print 'Access code: ' + access_token
    global token_type
    token_type = response_json['token_type']
    global refresh_token
    refresh_token = response_json['refresh_token']


def create_auth_header():
    return {
        'Authorization': token_type + ' ' + access_token,
        'Content-Type': 'application/json'
    }


def create_multipart_auth_header():
    return {
        'Authorization': token_type + ' ' + access_token,
        'Content-Type': 'multipart/form-data'
    }


def auth_header():
    return 'Authorization: ' + token_type + ' ' + access_token
