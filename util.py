import os
from docx import Document
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A3, landscape
import urllib2
import datetime


def create_text_file(name):
    print 'Creating new text file: ' + name
    try:
        new_file = open(name, 'w')
        new_file.write('yoyo this is a test text file!\n' + name)
        new_file.close()
        size = os.stat(name).st_size
        return size
    except:
        print 'Something went wrong, cannot create file\n' + name


def create_doc_file(name):
    document = Document()
    document.add_paragraph('d-o-c-x test fileeee: ' + name)
    document.save(name)


def create_pdf_file(name):
    c = canvas.Canvas(name)
    c.drawString(100, 700, name)
    c.showPage()
    c.save()


def get_image(filename, url):
    if not os.path.exists(filename):
        response = urllib2.urlopen(url)
        f = open(filename, 'w')
        f.write(response.read())
        f.close()


# yyyy-MM-ddTHH:mm:ss.sssZ
def get_datetime_now():
    return datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'


def get_datetime_later():
    two_hours_from_now = datetime.datetime.now() + datetime.timedelta(hours=2)
    return two_hours_from_now.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'